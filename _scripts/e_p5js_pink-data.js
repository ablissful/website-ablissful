let regen = true;
let rand;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");

  noStroke();
}

function draw() {
  if (regen) {
    fill("#ffe5ec");
    square(0, 0, 1000000000);

    for (let x = 0; x < width + 10; x += 10) {
      for (let y = 0; y < height + 10; y += 20) {
        rand = random([5, 5, 5, 10, 10, 10, 10, 10, 20, 20, 20, 20, 20]);

        if (rand == 5) {
          fill("#ffc2d1");
          circle(x, y, 5);
        } else if (rand == 10) {
          fill("#ffb3c6");
          circle(x, y, 10);
        } else {
          fill("#FFA0B8");
          circle(x, y, 15);
        }
      }
    }
    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}
