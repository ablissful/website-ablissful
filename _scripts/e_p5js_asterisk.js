let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");

  strokeWeight(10);
  strokeCap(SQUARE);

  angleMode(DEGREES);
}

function draw() {
  if (regen) {
    noStroke();
    fill(255);
    square(0, 0, 720);

    for (let x = -100; x < width; x += 50) {
      for (let y = -100; y < height; y += 50) {
        stroke(random(["#f68eab", "#ed225d", "#840b2d"]));

        push();

        translate(x + random(0, width / 15), y + random(0, height / 15));
        rotate(random(-20, 20));
        scale(random(0.25, 1.5));

        line(25, 25, 25, 0);
        line(25, 25, 45, 15);
        line(25, 25, 40, 40);
        line(25, 25, 10, 40);
        line(25, 25, 5, 15);

        pop();
      }
    }
  }
  regen = false;
}

function regenerate() {
  clear();
  regen = true;
}
