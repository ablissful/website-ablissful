let regen = true;

let x;
let y;

let ringCount;
let diameter;

let count = 0;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");

  noStroke();
  noFill();
}

function draw() {
  if (regen) {
    for (let e = 0; e < 200; e += 1) {
      x = random(0, width);
      y = random(0, height);

      diameter = random(100, 200);
      ringCount = random(5, 10);

      for (let i = 0; i < ringCount; i++) {
        if (count % 2 == 0) {
          fill(
            random([
              color(245, 143, 167),
              color(255, 224, 153),
              color(116, 251, 215),
              color(124, 213, 243),
            ])
          );
        } else {
          fill(color(255));
        }

        circle(x, y, diameter - (diameter / ringCount) * i);

        count++;
      }

      count = 0;
    }
    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}
