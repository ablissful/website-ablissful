let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");
}

function draw() {
  if (regen) {
    // Background substitute
    fill(235, 255, 244);
    noStroke();
    square(0, 0, 720);

    for (let y = 0; y < height; y += 10) {
      for (let x = 0; x < width; x += 10) {
        chance = random(1, 100);

        if (chance < 35) {
          stroke(color(240, 233, 209));
        } else if (chance > 35 && chance < 65) {
          stroke(color(200, 213, 134));
        } else if (chance > 65 && chance < 95) {
          stroke(color(162, 184, 101));
        } else if (chance > 95) {
          stroke(color(191, 116, 111));
        }

        strokeWeight(random([1, 2, 3, 4, 5]));
        line(
          random(5, 10) + x,
          random(1, 10) + y,
          random(1, 15) + x,
          random(10, 45) + y
        );
      }
    }
    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}
