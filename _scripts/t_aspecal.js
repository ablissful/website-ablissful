/*
 * Title:       Aspect Ratio Calculator (Aspecal)
 * Author:      ablissful
 * Version:     0.0.1
 * Modified:    27 July 2024
 * Description: Calculates the height or width of a rectangle by a specified aspect ratio.
 * 
 * Copyright (C) 2024 ablissful
`*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* # TODO
 * - Add Math.floor() as an option.
 */

// Document Queries
const SELECT_AR = document.getElementById("select-aspect-ratio");
const SECTION_CUSTOM_AR = document.getElementById(
  "section-custom-aspect-ratio"
);

const CUSTOM_HEIGHT = document.getElementById("custom-height");
const CUSTOM_WIDTH = document.getElementById("custom-width");
const OUTPUT_CUSTOM_AR = document.getElementById("custom-aspect-ratio-output");

const FIELD_HEIGHT = document.getElementById("field-height");
const FIELD_WIDTH = document.getElementById("field-width");

// Variables
let get_width;
let get_height;

let value_ar = 0;

//Initializes the program with SELECT_AR.value set to 0.
window.onload = function () {
  SELECT_AR.value = 0;
};

// Gives value_ar based on SELECT_AR.
SELECT_AR.addEventListener("change", function () {
  clearValues();
  switch (this.value) {
    case "1" /* Film - 21:9 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 2.333;
      break;
    case "2" /* Film - 16:9 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 1.777;
      break;
    case "3" /* Film - 4:3 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 1.333;
      break;
    case "4" /* Paper - 11:8.5 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 1.294;
      break;
    case "5" /* Paper - 11:17 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 1.545;
      break;
    case "6" /* Gameboy Camera - 1:14 */:
      SECTION_CUSTOM_AR.style.display = "none";
      value_ar = 1.14;
      break;
    case "7" /* Custom */:
      SECTION_CUSTOM_AR.style.display = "block";
      value_ar = null;
      break;
    default:
      console.log("err - case out of bounds");
  }
});

// Calculates height based on width.
document.getElementById("calculate-height").onclick = (event) => {
  get_width = FIELD_WIDTH.value;
  FIELD_HEIGHT.value = get_width / value_ar;
  FIELD_HEIGHT.value = Math.round(FIELD_HEIGHT.value);
};

// Calculates width based on height.
document.getElementById("calculate-width").onclick = (event) => {
  get_height = FIELD_HEIGHT.value;
  FIELD_WIDTH.value = get_height / value_ar;
  FIELD_WIDTH.value = Math.round(FIELD_WIDTH.value);
};

// Calculates aspect ratio based on CUSTOM_WIDTH and CUSTOM_HEIGHT
document.getElementById("calculate-ar").onclick = (event) => {
  get_height = Number(CUSTOM_HEIGHT.value);
  get_width = Number(CUSTOM_WIDTH.value);

  if (get_height > get_width) {
    value_ar = get_height / get_width;
  } else if (get_height < get_width) {
    value_ar = get_width / get_height;
  } else if (get_height === get_width) {
    value_ar = 1;
  }

  OUTPUT_CUSTOM_AR.value = value_ar;
};

// Clears the values in the HTML input fields.
function clearValues() {
  CUSTOM_HEIGHT.value = "";
  CUSTOM_WIDTH.value = "";
  OUTPUT_CUSTOM_AR.value = "";
  FIELD_HEIGHT.value = "";
  FIELD_WIDTH.value = "";
}
