let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(700, 700);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");

  angleMode(DEGREES);
  noStroke();
}

function draw() {
  if (regen) {
    // Background Fix
    fill("#A098C6");
    square(0, 0, 700);
    let x,
      y = -50;
    let x2,
      y2 = -25;
    let polar = 1;
    let rand;

    blendMode(OVERLAY);

    translate(-25, -25);

    /* Layer One */
    fill("#B6A6CA");
    for (y2; y2 < height + 25; y2 += 12.5) {
      for (x2; x2 < width + 25; x2 += 25) {
        rand = random([1, 2]);

        if (rand == 1) {
          beginShape();
          vertex(12.5 + x2, 0 + y2);
          vertex(25 + x2, 12.5 + y2);
          vertex(12.5 + x2, 25 + y2);
          vertex(0 + x2, 12.5 + y2);
          endShape(CLOSE);
        }
      }

      polar *= -1;

      if (polar == 1) {
        x2 = 0;
      } else {
        x2 = 12.5;
      }
    }

    /* Layer Two */
    fill("#D5CFE1");

    for (y; y < height + 25; y += 25) {
      for (x; x < width + 25; x += 50) {
        rand = random([1, 2]);

        if (rand == 1) {
          beginShape();
          vertex(25 + x, 0 + y);
          vertex(50 + x, 25 + y);
          vertex(25 + x, 50 + y);
          vertex(0 + x, 25 + y);
          endShape(CLOSE);
        }
      }

      polar *= -1;

      if (polar == 1) {
        x = 0;
      } else {
        x = 25;
      }
    }

    blendMode(REPLACE);

    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}
