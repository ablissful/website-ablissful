let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");
}

function draw() {
  if (regen == true) {
    // Background substitute
    fill(232, 252, 207);
    noStroke();
    square(0, 0, 720);

    // Color settings
    fill(249, 160, 63);
    stroke(150, 224, 114);

    // Generative art
    for (let i = 0; i < 720; i += 5) {
      strokeWeight(random(1, 3));
      line(-20 + i + random(-40, 40), -20, -20 + i + random(1, 20), 740);
      circle(random(0, 720), random(0, 720), random(5, 10));
    }
  }
  regen = false;
}

function regenerate() {
  clear();
  regen = true;
}
