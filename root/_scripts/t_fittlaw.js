/*
 * Title:       Fitts' Law Calculator (Fittlaw)
 * Author:      ablissful
 * Version:     0.0.2
 * Modified:    27 November 2024
 * Description: Calculates the height or width of a rectangle by a specified aspect ratio.
 * 
 * Copyright (C) 2024 ablissful
`*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* # TODO
 * -
 */

const A_VALUE = document.getElementById("input-a");
const B_VALUE = document.getElementById("input-b");
const AMPLITUDE_VALUE = document.getElementById("input-amplitude");
const WIDTH_VALUE = document.getElementById("input-width");
const ID_VALUE = document.getElementById("input-id");

let get_a, get_b, get_amp, get_width, get_id;

const OUTPUT_MT = document.getElementById("output-mt");
const OUTPUT_ID = document.getElementById("output-id");

const BUTTON_MT = document.getElementById("button-mt");
const BUTTON_ID = document.getElementById("button-id");

const SELECT_PRODUCT = document.getElementById("option-product");

const SECTION_INPUT_ID = document.getElementById("section-input-id-values");
const SECTION_INPUT_MT = document.getElementById("section-input-mt-values");

const SECTION_OUTPUT_ID = document.getElementById("section-output-id-value");
const SECTION_OUTPUT_MT = document.getElementById("section-output-mt-value");

//Initializes the program with SELECT_PRODUCT.value set to 1.
window.onload = function () {
  SELECT_PRODUCT.value = 1;

  SECTION_INPUT_ID.style.display = "grid";
  SECTION_INPUT_MT.style.display = "none";

  SECTION_OUTPUT_ID.style.display = "grid";
  SECTION_OUTPUT_MT.style.display = "none";
};

// Gives value_ar based on SELECT_PRODUCT.
SELECT_PRODUCT.addEventListener("change", function () {
  switch (this.value) {
    case "1" /* Index of Difficulty */:
      SECTION_INPUT_ID.style.display = "grid";
      SECTION_INPUT_MT.style.display = "none";

      SECTION_OUTPUT_ID.style.display = "grid";
      SECTION_OUTPUT_MT.style.display = "none";
      break;
    case "2" /* Time Movement */:
      SECTION_INPUT_ID.style.display = "none";
      SECTION_INPUT_MT.style.display = "grid";

      SECTION_OUTPUT_ID.style.display = "none";
      SECTION_OUTPUT_MT.style.display = "grid";
      break;
    default:
      console.log("err - case out of bounds");
  }
});

BUTTON_ID.onclick = (event) => {
  get_amp = parseInt(AMPLITUDE_VALUE.value);
  get_width = parseInt(WIDTH_VALUE.value);

  OUTPUT_ID.value = Math.log2(1 + get_amp / get_width);
};

BUTTON_MT.onclick = (event) => {
  get_a = parseInt(A_VALUE.value);
  get_b = parseInt(B_VALUE.value);
  get_id = parseFloat(ID_VALUE.value);

  OUTPUT_MT.value = get_a + get_b * get_id;
  console.log(OUTPUT_MT.value);
};
