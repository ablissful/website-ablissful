let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");
}

function draw() {
  if (regen == true) {
    // Background Substitute
    fill(134, 231, 184);
    noStroke();
    square(0, 0, 720);

    for (let i = 0; i < 1000; i += 1) {
      fill(
        random([
          color(134, 231, 184),
          color(147, 255, 150),
          color(178, 255, 168),
          color(208, 255, 183),
          color(242, 245, 222),
        ])
      );

      stroke(
        random([
          color(134, 231, 184),
          color(147, 255, 150),
          color(178, 255, 168),
          color(208, 255, 183),
          color(242, 245, 222),
        ])
      );

      strokeWeight(random([8, 16, 32]));

      rotate(random(0, QUARTER_PI));
      square(random(0, width), random(0, height), random(50, 100));
    }
  }
  regen = false;
}

function regenerate() {
  clear();
  regen = true;
}
