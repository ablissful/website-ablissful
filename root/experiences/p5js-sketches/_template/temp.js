let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(720, 720);
  myCanvas.parent("sketch");
  myCanvas.style("width", "100%");
  myCanvas.style("height", "");
}

function draw() {
  if (regen) {
    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}
