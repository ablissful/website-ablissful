let regen = true;

function setup() {
  // Responsive Canvas
  let myCanvas = createCanvas(500, 500, SVG);
  myCanvas.parent("sketch");
}

function draw() {
  if (regen == true) {
    noStroke();
    fill("#ff9933");
    circle(width / 2, height / 2, 50);
    regen = false;
  }
}

function regenerate() {
  clear();
  regen = true;
}

function exportSVG() {
  save(
    "svg-temp_" + day() + "-" + month() + "-" + year() + "_" + millis() + ".svg"
  );
  print("SVG Downloaded");
}
